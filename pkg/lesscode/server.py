# -*- coding: utf-8 -*-
from lesscode.web.web_server import WebServer

if __name__ == "__main__":
    server = WebServer()
    server.start()
