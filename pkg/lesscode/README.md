# lesscode-py
## 框架涉及的包：
```
  "tornado==6.0",
  "tornado-sqlalchemy==0.7.0",
  "aiomysql==0.0.22",
  "motor==2.5.1",
  "elasticsearch==7.15.2",
  "aiohttp==3.8.1",
  "crypto==1.4.1",
  "pycryptodome==3.12.0",
  "aioredis==2.0.1",
  "DBUtils==3.0.2",
  "redis==4.1.4",
  "requests==2.27.1",
  "neo4j==5.0.0",
  "snowland-smx==0.3.1",
  "py_eureka_client==0.11.3",
  "ks3sdk==1.5.0",
  "filechunkio==1.8",
  "APScheduler==3.9.1",
  "nacos-sdk-python==0.1.8",
  "pika==1.3.0",
  "kafka-python==2.0.2"
  "aiopg>=1.3.3"  # 除了pg库没安装，以上包均已安装，如有需要自行安装
```
  
