# lesscodeTool

#### Description
```
Usage: lesscodeTool [OPTIONS] COMMAND [ARGS]...

  低代码构建工具.

Options:
  --help  Show this message and exit.

Commands:
  new          新建一个项目,目前进支持lesscode-py模板
  sqlacodegen  生成SQLALCHEMY模型类
  subcommand   执行系统命令
  swagger      swagger api转换
```